const React = require('react');
require('../style/css/tweet-map-component.css');
const moment = require('moment');

var mapboxgl = require('mapbox-gl/dist/mapbox-gl.js');
mapboxgl.accessToken = 'pk.eyJ1IjoibWFwMTcxIiwiYSI6ImNqanNrdmxmMTEwMjQza3BpOWJodDJ6d3QifQ.pUNyXx8-84BpDzA_Q6xlVw';

class TweetMapComponent extends React.Component {

    constructor(props) {
        super(props);
        this.currentMarkers = [];
        this.map = null;
    }

    componentDidMount() {
        this.map = new mapboxgl.Map({
            container: 'tweetMap',
            style: 'mapbox://styles/mapbox/streets-v9',
            center: [2.349014, 48.864716],
            zoom: 1,
        });
        this.map.addControl(new mapboxgl.NavigationControl({ position: 'top-right' }));
    }

    handleClear() {
        this.props.clearGeoTweets();
        this.currentMarkers.forEach(marker => {
            marker.remove();
        });
    }

    getInitialState() {
        return {
            tweets: [],
        };
    }

    plotMarker(tweet) {
        const markerJson = {
            type: 'Feature',
            geometry: {
                type: 'Point',
                coordinates: tweet.coordinates.coordinates,
            },
            properties: {
                date: tweet.created_at,
                title: tweet.user.name,
                avatar: tweet.user.avatar,
                description: tweet.text,
            }
        };
        // create a HTML element for each feature
        var el = document.createElement('div');
        el.className = 'marker';

        // make a marker for each feature and add to the map
        const newMarker = new mapboxgl.Marker(el)
            .setLngLat(markerJson.geometry.coordinates)
            .setPopup(new mapboxgl.Popup({ offset: 25 }) // add popups
                .setHTML(this.getTweetPopUpHTML(
                    markerJson.properties.date,
                    markerJson.properties.title,
                    markerJson.properties.avatar,
                    markerJson.properties.description))
            );
        newMarker.addTo(this.map);
        this.currentMarkers.push(newMarker);
    }

    getTweetPopUpHTML(date, title, avatar, description) {
        let html = '';
        html += '<article class="media">';
        html += '<figure class="media-left">';
        html += '<p class="image is-64x64 is-circle">';
        html += '<img src="' + avatar + '"/>';
        html += '</p>';
        html += '</figure>'
        html += '<div class="media-content">';
        html += '<div class="content">';
        html += '<p>'
        html += '<strong>' + title + '</strong> <small>' + moment(date).format('Do MMM, H:m:s') + '</small>';
        html += '<br></br>'
        html += this.convertUrlsToLink(description);
        html += '</p>';
        html += '</div>';
        html += '</div>'
        html += '</article>';
        return html;
    }

    convertUrlsToLink(text) {
        const exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
        const temp = text.replace(exp, '<a target="_blank" href="$1">$1</a>');
        var exp2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
        const result = temp.replace(exp2, '$1<a target="_blank" href="http://$2">$2</a>');
        return result;
    }

    render() {
        return (
            <div id='tweetMap'>
                <a id="clearMapButton" className="button is-danger" onClick={this.handleClear.bind(this)}>
                    <span className="icon is-small">
                        <i className="fas fa-sync-alt"></i>
                    </span>
                    <span> Clear Geo Tweets</span>
                </a>
            </div>
        )

    }

}

module.exports = TweetMapComponent;