require('../style/css/tag-item.css');

const React = require('react');

class TagItem extends React.Component {

    handleDelete() {
        this.props.onDelete(this.props.item, this.props.tagClass);
    }

    render() {
        return (
            <span className="tag is-primary">
                {this.props.tagClass}{this.props.item}
                <button className="delete" onClick={this.handleDelete.bind(this)}></button>
            </span>
        )
    }
}

module.exports = TagItem;