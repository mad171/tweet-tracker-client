require('../style/css/search-component.css');

const React = require('react');

const TagItem = require('./tag-item');


class SearchComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = this.getInitialState();
        this.tracking = '';
    }

    getInitialState() {
        return {
            search: {
                text: [],
                hashtag: [],
                at: [],
            }
        };
    }

    handleSubmit(e) {
        e.preventDefault();
        this.addSearchTerms(this.refs.newText.value, this.refs.newHashtag.value, this.refs.newAt.value);
        this.clearInputs();
    }

    clearInputs() {
        this.refs.newText.value = "";
        this.refs.newHashtag.value = "";
        this.refs.newAt.value = "";
    }

    addSearchTerms(text, hashtag, at) {
        let updatedSearch = this.state.search;
        text = text.replace(/\s/g, "");
        hashtag = hashtag.replace(/\s/g, "");
        at = at.replace(/\s/g, "");
        if (text !== "" && !updatedSearch.text.includes(text)) {
            updatedSearch.text.push(text);
            this.setState({ search: updatedSearch, });
            this.props.updateSearch(this.state.search);
        }
        if (hashtag !== "" && !updatedSearch.hashtag.includes(hashtag)) {
            updatedSearch.hashtag.push(hashtag);
            this.setState({ search: updatedSearch, });
            this.props.updateSearch(this.state.search);
        }
        if (at !== "" && !updatedSearch.at.includes(at)) {
            updatedSearch.at.push(at);
            this.setState({ search: updatedSearch, });
            this.props.updateSearch(this.state.search);
        }
    }

    deleteSearchTerm(item, tagClass) {

        let updatedSearch = this.state.search;
        if (tagClass === '') {
            updatedSearch.text = this.state.search.text.filter(function (val, index) {
                return item !== val;
            });
        }
        else if (tagClass === '#') {
            updatedSearch.hashtag = this.state.search.hashtag.filter(function (val, index) {
                return item !== val;
            });
        }
        else if (tagClass === '@') {
            updatedSearch.at = this.state.search.at.filter(function (val, index) {
                return item !== val;
            });
        }
        this.setState({ search: updatedSearch });
        this.props.updateSearch(this.state.search);
    }

    deleteAllSearchTerms() {
        let updatedSearch = this.state.search;
        updatedSearch.text = [];
        updatedSearch.hashtag = [];
        updatedSearch.at = [];
        this.setState({ search: updatedSearch });
        this.props.updateSearch(this.state.search);
    }

    getListOfItems(tagClass) {
        let list;
        if (tagClass === '') { list = this.state.search.text; }
        else if (tagClass === '#') { list = this.state.search.hashtag; }
        else if (tagClass === '@') { list = this.state.search.at; }
        return list.map(function (item, index) {
            return (
                <TagItem tagClass={tagClass} item={item} key={index} onDelete={(item, tagClass) => this.deleteSearchTerm(item, tagClass)} />
            );
        }, this);
    }

    render() {
        const listText = this.getListOfItems('');
        const listHashtag = this.getListOfItems('#');
        const listAt = this.getListOfItems('@');
        return (
            <div id="searchPanel">
                <form id="addSearchForm" onSubmit={this.handleSubmit.bind(this)}>
                    <span className="icon is-medium" onClick={this.deleteAllSearchTerms.bind(this)}>
                        <i className="far fa-stop-circle fa-2x"></i>
                    </span>
                    <div className="control">
                        <input className="input is-small is-rounded" placeholder="Track a text ?" ref="newText" />
                    </div>
                    <div className="control has-icons-left">
                        <input className="input is-small is-rounded" placeholder="Track a # ?" ref="newHashtag" />
                        <span className="icon is-small is-left">
                            <i className="fas fa-hashtag"></i>
                        </span>
                    </div>
                    <div className="control has-icons-left">
                        <input className="input is-small is-rounded" placeholder="Track a @ ?" ref="newAt" />
                        <span className="icon is-small is-left">
                            <i className="fas fa-at"></i>
                        </span>
                    </div>
                    <div className="control">
                        <button className="button is-primary is-rounded" type='submit' value="Hit me">
                            <span className="icon">
                                <i className="fab fa-twitter"></i>
                            </span>
                            <span>Track them !</span>
                        </button>
                    </div>
                </form>
                <div id="tagsList" className="tags">{listText}{listHashtag}{listAt}</div>
            </div>
        );
    }
}

module.exports = SearchComponent;