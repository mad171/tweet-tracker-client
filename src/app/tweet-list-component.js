require('../style/css/tweet-list-component.css');
const React = require('react');
const moment = require('moment');

class TweetListComponent extends React.Component {

    handleClear() {
        this.props.clearTweets();
    }


    render() {
        const listTweets = this.props.tweets.map(function (item, index) {
            return (
                <article key={index} className="media">
                    <figure className="media-left">
                        <p className="image is-64x64 is-circle">
                            <img src={item.user.avatar} />
                        </p>
                    </figure>
                    <div className="media-content">
                        <div className="content">
                            <p>
                                <strong>{item.user.name}</strong> <small>{moment(item.created_at).format('Do MMM, H:m:s')}</small>
                                <br></br>
                                {item.text}
                            </p>
                        </div>
                    </div>
                </article>
            );
        }, this);
        return (
            <div id="listPanel">
                <h1 id="listTitle" className="subtitle is-3">Tweet List</h1>
                <ul id="tweetList" className="tweets card-content">
                    {listTweets}
                </ul>
                <a id="clearListButton" className="button is-danger" onClick={this.handleClear.bind(this)}>
                    <span className="icon is-small">
                        <i className="fas fa-sync-alt"></i>
                    </span>
                    <span> Clear Tweet List</span>
                </a>
            </div>
        );
    }
}

module.exports = TweetListComponent;