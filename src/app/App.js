import '../style/css/App.css';
const React = require('react');
const socketIOClient = require('socket.io-client');

const SearchComponent = require('./search-component');
const TweetListComponent = require('./tweet-list-component');
const TweetMapComponent = require('./tweet-map-component');
const TWEETS_QUEUE_SIZE = 20;
const TWEETS_GEO_QUEUE_SIZE = 50;


class App extends React.Component {

  constructor() {
    super();
    this.state = this.getInitialState();
    this.tracking = {
      text: [],
      hashtag: [],
      at: [],
    };
    this.socket = socketIOClient('http://localhost:3000/');
  }

  getInitialState() {
    return ({
      tweets: [],
      geoTweets: [],
    })
  }

  componentDidMount() {
    this.setSocketTracker();
    window.addEventListener("beforeunload", (ev) => {
      ev.preventDefault();
      this.closeAllStreams();
      return ev.returnValue = 'Are you sure you want to close?';
    });
  }

  setSocketTracker() {
    this.socket.on('connect', () => {
      console.log("Socket client Connected");
      this.socket.on("new-tweet", newTweet => {
        const updatedTweets = this.state.tweets;
        updatedTweets.unshift(newTweet);
        if (updatedTweets.length > TWEETS_QUEUE_SIZE) { updatedTweets.pop(); }
        this.setState({ tweets: updatedTweets });
        if (newTweet.coordinates !== null) {
          const updatedGeoTweets = this.state.geoTweets;
          updatedGeoTweets.unshift(newTweet);
          if (updatedGeoTweets.length > TWEETS_GEO_QUEUE_SIZE) { updatedGeoTweets.pop(); }
          this.setState({ geoTweets: updatedGeoTweets });
          this.refs.mapComponent.plotMarker(newTweet);
        }
      });
    });
    this.socket.on('disconnect', () => {
      this.socket.off("new-tweet")
      this.socket.removeAllListeners("new-tweet");
      console.log("Socket client Disconnected");
    });
  }

  onSearchUpdate(newSearch) {
    console.log('onSearchUpdate !!');
    console.log(newSearch);
    // Update Text Tags
    if (this.tracking.text !== newSearch.text) {
      // Check whether a text tag has been added... 
      newSearch.text.forEach(element => {
        if (!this.tracking.text.includes(element)) {
          this.tracking.text.push(element);
          this.socket.emit('subscribe', element);
        }
      });
      // ... or deleted
      this.tracking.text.forEach(element => {
        if (!newSearch.text.includes(element)) {
          this.tracking.text = this.tracking.text.filter(e => e !== element);
          this.socket.emit('unsubscribe', element);
        }
      });
    }

    // Update Hashtag Tags
    if (this.tracking.hashtag !== newSearch.hashtag) {
      // Check whether a # tag has been added... 
      newSearch.hashtag.forEach(element => {
        if (!this.tracking.hashtag.includes(element)) {
          this.tracking.hashtag.push(element);
          this.socket.emit('subscribe', '#' + element);
        }
      });
      // ... or deleted
      this.tracking.hashtag.forEach(element => {
        if (!newSearch.hashtag.includes(element)) {
          this.tracking.hashtag = this.tracking.hashtag.filter(e => e !== element);
          this.socket.emit('unsubscribe', '#' + element);
        }
      });
    }

    // Update At Tags
    if (this.tracking.at !== newSearch.at) {
      // Check whether a @ tag has been added...
      newSearch.at.forEach(element => {
        if (!this.tracking.at.includes(element)) {
          this.tracking.at.push(element);
          this.socket.emit('subscribe', '@' + element);
        }
      });
      // ... or deleted
      this.tracking.at.forEach(element => {
        if (!newSearch.at.includes(element)) {
          this.tracking.at = this.tracking.at.filter(e => e !== element);
          this.socket.emit('unsubscribe', '@' + element);
        }
      });
    }

  }

  closeAllStreams() {
    this.tracking.text.forEach(element => {
      this.socket.emit('unsubscribe', element);
    });
    this.tracking.hashtag.forEach(element => {
      this.socket.emit('unsubscribe', '#' + element);
    });
    this.tracking.at.forEach(element => {
      this.socket.emit('unsubscribe', '@' + element);
    });
    this.tracking = {};
  }

  onClearTweets() {
    this.setState({ tweets: [] });
  }
  onClearGeoTweets() {
    this.setState({ geoTweets: [] });
  }

  render() {
    return (
      <div id="myApp">
        <TweetListComponent id="listPanel" tweets={this.state.tweets} clearTweets={this.onClearTweets.bind(this)} />
        <div id="rightPanel">
          <div id="titleBanner">
            <i className="fab fa-twitter fa-3x"></i>
            <h1 className="title is-1">Tweet-Tracker</h1>
            <i className="fab fa-twitter fa-3x fa-flip-horizontal"></i>
          </div>
          <SearchComponent id="searchPanel" updateSearch={this.onSearchUpdate.bind(this)} />
          <TweetMapComponent id="mapPanel" ref="mapComponent" clearGeoTweets={this.onClearGeoTweets.bind(this)} />
        </div>
      </div>
    );
  }

}

export default App;