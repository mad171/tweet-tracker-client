import React from 'react';
import ReactDOM from 'react-dom';
import './style/css/index.css';
import App from './app/App';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App id="totalApp"/>, document.getElementById('root'));
registerServiceWorker();
