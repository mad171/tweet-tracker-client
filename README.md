# Tweet Tracker

Tweet Tracker is a Web Application which provides you the ability to track topics on tweets posted in real time. It also plots the tweets on a map whenever the location is included by the tweeter.

![Screenshot of the app](./images/Capture.png)
[Download a video showing the app layout](./images/tweetmap.avi)

You can track topics in three ways:
  - Track words: whenever the word appears in a tweet content, this tweet will appear on your list and map (if we know its location)
  - Track @ : whenever your topic is tagged with a @, the tweet appears
  - Track # : whenever your hashtag appears in a tweet, it appears too


# Run it yourself

Create a root folder where you install the application: `mkdir Root`

Open a terminal and place yourself in your `/Root` folder `cd ./Root` and install the backend server:
  - `git clone https://gitlab.com/mad171/Tweet-Tracker-Back/`
  - `touch .env`
  - Edit the .env on the same way as .env.sample (fake credentials) file with your Tweeter Developer credentials so you can consume the streaming data it provides. You can follow the guide at [this link](https://developer.twitter.com/en/docs/tutorials/consuming-streaming-data) to get new credentials.
  - Make sure you have node and npm installed : [Download link](https://nodejs.org/en/download/)
  - Once you are done, just install the needed packages with `npm install` and then you're good to run it using `npm start`

Now open a new terminal and place yourslef again in your `/Root` folder `cd ./Root` and install the client app: 
  - `git clone https://gitlab.com/mad171/tweet-tracker-client/`
  - `touch .env`
  - Edit the .env on the same way as .env.sample (fake credentials) file with the port you want to serve the app on. You can keep it 3001 if it does not disturb your config.
  - Once you are done, just install the needed packages with `npm install` and then you're good to run it using `npm start`

You're good to track any topics on Tweeter !!

![Tweet Tracker Gif](./images/tweetmap.gif)