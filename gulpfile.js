// Requis
var gulp = require('gulp');

// Include plugins
var plugins = require('gulp-load-plugins')(); // tous les plugins de package.json

// Variables de chemins
var source = './src'; // dossier de travail
var destination = './src'; // dossier à livrer

// Tâche "build" = LESS + autoprefixer + CSScomb + beautify (source -> destination)
gulp.task('css', function () {
    return gulp.src(source + '/style/scss/*.scss')
        .pipe(plugins.sass())
        .pipe(plugins.csscomb())
        .pipe(plugins.cssbeautify({ indent: '  ' }))
        .pipe(plugins.autoprefixer())
        .pipe(gulp.dest(destination + '/style/css'));
});



// Tâche "build"
gulp.task('build', ['css']);

// Tâche "watch" = je surveille *less
gulp.task('watch', function () {
    gulp.watch(source + '/style/scss/*.scss', ['build']);
});

// Tâche par défaut
gulp.task('default', ['build']);